#ifndef SFE_AST_H
#define SFE_AST_H

#include <memory>
#include <vector>
#include <llvm/IR/Value.h>


using namespace std;

namespace sfeAST {
	class ASTNode {
	public:
		virtual ~ASTNode() = default;

	protected:
		ASTNode() = default;
	};

	class Statement : public ASTNode {
	public:
		virtual void translateToLLVM() = 0;

	  protected:
		Statement() = default;
	};

	class Expression : public ASTNode {
	public:
		virtual llvm::Value *getLLVMValue() = 0;
	protected:
		Expression() = default;
	};

	class Number : public Expression {
	public:
		Number(int val);

		llvm::Value *getLLVMValue() final;
		const int val;
	};
	class String : public Expression {
	public:
		String(const string text);

		llvm::Value *getLLVMValue() final;
		const string text;
	};


	class Body : public ASTNode {
	  public:
		Body(vector<unique_ptr<Statement>> statements);

		llvm::Value *codegen();

		const vector<unique_ptr<Statement>> statements;
	};


	class Type {
	public:
		virtual llvm::Type *getLLVMType() = 0;
		virtual llvm::Constant *getInitValue() = 0;
	protected:
		Type() = default;
	};
	class
	Integer : public Type {
	public:
		Integer() = default;
		llvm::Type *getLLVMType();
		llvm::Constant *getInitValue() final;
	};
	class Array : public Type {
	public:
		Array(unique_ptr<Number> min, unique_ptr<Number> max, shared_ptr<Type> type);
		llvm::Type *getLLVMType();
		llvm::Constant *getInitValue() final;


		const unique_ptr<Number> minIndex, maxIndex;
		const shared_ptr<Type> type;
	};

	class Declaration : public ASTNode{
	  public:
		virtual ~Declaration() = default;
		virtual void translateToLLVM(bool isGlobal = false) = 0;
	  protected:
		Declaration() = default;
	};

	class VarDeclaration : public Declaration {
	public:
		VarDeclaration(const string &name, shared_ptr<Type> type);

		void translateToLLVM(bool isGlobal = false) final;

		const shared_ptr<Type> type;
		const string identifier;
	};

	class ConstDeclaration : public Declaration {
	public:
		ConstDeclaration(const string &name, int val);

		void translateToLLVM(bool isGlobal = false) final;

		const string identifier;
		const int value;
	};

	class FunctionProtoDeclaration {
	public:
		FunctionProtoDeclaration(const string &name, vector<unique_ptr<VarDeclaration>> params, shared_ptr<Type> retType);

		llvm::Function *codegen();

		const string name;
		const vector<unique_ptr<VarDeclaration>> params;
		const shared_ptr<Type> retType;
	};

	class ProcedureProtoDeclaration{
	public:
		ProcedureProtoDeclaration(const string &name, vector<unique_ptr<VarDeclaration>> params);

		llvm::Function *codegen();

		const string name;
		const vector<unique_ptr<VarDeclaration>> params;
	};

	class FunctionDeclaration : public Declaration {
	public:
		FunctionDeclaration(unique_ptr<FunctionProtoDeclaration> proto, vector<unique_ptr<Declaration>> declarations,
							unique_ptr<Body> body);

		void translateToLLVM(bool isGlobal = false) final;

		const unique_ptr<FunctionProtoDeclaration> proto;
		const vector<unique_ptr<Declaration>> declarations;
		const unique_ptr<Body> body;
	};

	class ProcedureDeclaration : public Declaration {
	public:
		ProcedureDeclaration(unique_ptr<ProcedureProtoDeclaration> proto, vector<unique_ptr<Declaration>> declarations,
							 unique_ptr<Body> body);

		void translateToLLVM(bool isGlobal = false) final;

		const unique_ptr<ProcedureProtoDeclaration> proto;
		const vector<unique_ptr<Declaration>> declarations;
		const unique_ptr<Body> body;
	};

	class Program : public ASTNode {
	public:
		Program(const string &name, vector<unique_ptr<Declaration>> declarations, unique_ptr<Body> body);

		void translateToLLVM();
		void getLLVM();
		void printModule();
		void saveToObjectFile(const string & filename);
		const string name;
		const vector<unique_ptr<Declaration>> declarations;
		const unique_ptr<Body> body;
	};

	class Break : public Statement{
		void translateToLLVM() final;
	};

	class Exit : public Statement {
		void translateToLLVM() final;
	};

	class VarReference : public Expression {
	public:
		virtual llvm::Value *getLLVMValue() = 0;
		virtual llvm::Value * getLLVMAddress() = 0;
	protected:
		VarReference() = default;
	};

	class SimpleVarReference : public VarReference {
	public:
		SimpleVarReference(const string &name);

		llvm::Value *getLLVMValue() final;
		llvm::Value * getLLVMAddress() final;
		const string name;
	};

	class ArrayItemReference : public VarReference {
	public:
		ArrayItemReference(unique_ptr<VarReference> var, unique_ptr<Expression> index);

		llvm::Value *getLLVMValue() final;
		llvm::Value * getLLVMAddress() final;

		const unique_ptr<VarReference> var;
		const unique_ptr<Expression> index;
	};

	class Assign : public Statement {
	public:
		Assign(unique_ptr<VarReference> var, unique_ptr<Expression> value);

		void translateToLLVM() final;

		const unique_ptr<VarReference> var;
		const unique_ptr<Expression> value;
	};

	class If : public Statement {
	public:
		If(unique_ptr<Expression> condition, unique_ptr<Body> then, unique_ptr<Body> elseBody);

		void translateToLLVM() final;

		const unique_ptr<Expression> condition;
		const unique_ptr<Body> then;
		const unique_ptr<Body> elseBody;
	};

	class For : public Statement {
	public:
		For(const string &varName, unique_ptr<Expression> startValue,
			unique_ptr<Expression> endValue, bool ascending, unique_ptr<Expression> by,
			unique_ptr<Body> body);

		void translateToLLVM() final;

		const string varName;
		const unique_ptr<Expression> startValue;
		const unique_ptr<Expression> endValue;
		const bool ascending;
		const unique_ptr<Expression> by;
		const unique_ptr<Body> body;
	};

	class While : public Statement {
	public:
		While(unique_ptr<Expression> condition, unique_ptr<Body> doBody);

		void translateToLLVM() final;

		const unique_ptr<Expression> condition;
		const unique_ptr<Body> body;
	};

	class ProcedureCall : public Statement {
	public:
		ProcedureCall(const string &name, vector<unique_ptr<Expression>> parameters);

		void translateToLLVM() final;

		const string name;
		const vector<unique_ptr<Expression>> parameters;
	};

	class FunctionCall : public Expression {
	public:
		FunctionCall(const string &name, vector<unique_ptr<Expression>> parameters);

		llvm::Value *getLLVMValue() final;

		const string name;
		const vector<unique_ptr<Expression>> parameters;
	};

	enum binOperation {
		bo_plus,
		bo_minus,
		bo_divide,
		bo_multiply,
		bo_modulo,
		bo_equal,
		bo_nonEqual,
		bo_less,
		bo_lessEqual,
		bo_greater,
		bo_greaterEqual,
		bo_or,
		bo_and
	};

	class BinOp : public Expression {
	public:
		BinOp(unique_ptr<Expression> left, unique_ptr<Expression> right, binOperation op);

		llvm::Value *getLLVMValue() final;

		const unique_ptr<Expression> left, right;
		const binOperation op;
	};

}


#endif //SFE_AST_H
