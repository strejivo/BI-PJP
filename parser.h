#ifndef SFE_PARSER_H
#define SFE_PARSER_H

#include <string>
#include <vector>
#include "lexan.h"
#include "AST.h"

using namespace std;
using namespace sfeAST;

class InvalidTokException : public exception {
public:
	InvalidTokException(const vector<Token> &expected, Token CurTok);

	string message() const;

	const vector<Token> expected;
	const Token CurTok;
};

class sfeParser {
private:
	sfeLexan lexan;

	Token CurTok;

	string lastIdentifier;

	int lastNumVal;

	string lastText;

	int getNextToken();

	void matchToken(Token expected);

public:

	sfeParser(std::string filename);

	shared_ptr<Type> parseType();

/// VarDeclaration
	vector<unique_ptr<VarDeclaration>> parseVarDeclaration();

	vector<unique_ptr<ConstDeclaration>> parseConstsDeclaration();

	vector<unique_ptr<VarDeclaration>> parseParamsDecl();

	unique_ptr<FunctionProtoDeclaration> parseFunctionProto();

	unique_ptr<ProcedureProtoDeclaration> parseProcedureProto();

	unique_ptr<Body> parseBody();

	unique_ptr<FunctionDeclaration> parseFunction();

	unique_ptr<ProcedureDeclaration> parseProcedure();

	unique_ptr<Program> parseProgram();

	unique_ptr<Number> parseNumber();

	unique_ptr<Exit> parseExit();

	unique_ptr<If> parseIf();

	unique_ptr<While> parseWhile();

	unique_ptr<For> parseFor();

	vector<unique_ptr<Expression>> parseCallParams();

	unique_ptr<ProcedureCall> parseProcedureCall(string name);

	unique_ptr<FunctionCall> parseFunctionCall(string name);

	unique_ptr<ArrayItemReference> parseArrayReference(unique_ptr<VarReference> base);

	unique_ptr<VarReference> parseVarReference(string name);

	unique_ptr<Expression> parseIdentifierExpression();

	unique_ptr<Expression> parseString();

	unique_ptr<Expression> parseLiteral();

	unique_ptr<Expression> parseSecondOrderExpression();

	unique_ptr<Expression> parseFirstOrderExpression();

	unique_ptr<Expression> parseExpression();

	unique_ptr<Assign> parseAssign(string name);

	unique_ptr<Assign> parseAssign();

	unique_ptr<Statement> parseIdentifierStatement();

	unique_ptr<Break> parseBreak();

	unique_ptr<Statement> parseStatement();
};

#endif //SFE_PARSER_H
