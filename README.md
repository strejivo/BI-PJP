# SFE Frontend
This is a semestral work for BI-PJP.

The Task was to implement frontend for either LLVM or GCC for sfe, simplified Pascal. The whole task is described [here](https://gitlab.fit.cvut.cz/pjp/semestralwork/blob/master/task_definition).

## Usage
You need to have installed LLVM 6.0.0. Then just run `cmake ./ & make`, that will produce the executable it self. You can run it `sfe $sfeSourceFile [-o $objFileName]`. This outputs `objFileName` (default is `output.o`) that you can compile using `g++` or `clang`. After this you have working executable based on your sfe source code.
Other possibility is to run it `sfe $sfeSourceFile -d`, this prints the LLVM IR on standart output. You can then run `lli` or `llc` (see what they do).
## TODO
Add support for multi-dimensional arrays (parser already does, but codegeneration relies on it being one-dimensional).
Add other types (double, char, strings as variable type)