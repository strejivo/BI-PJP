#include <algorithm>
#include <memory>
#include <iostream>
#include "lexan.h"
#include "parser.h"

InvalidTokException::InvalidTokException(const vector<Token> &expected, Token CurTok) : expected(expected),
																						CurTok(CurTok) {}

string InvalidTokException::message() const {
	string what = "Expected <";
	for (auto it : expected)
		what += to_string(it) + ";";
	if (!expected.empty()) what.erase(what.length() - 1);
	return (what + ">, given " + to_string(CurTok));
}

int sfeParser::getNextToken() {
	return CurTok = lexan.gettok();
}

void sfeParser::matchToken(Token expected) {
	if (CurTok != expected) throw InvalidTokException({expected}, CurTok);
	lastIdentifier = lexan.getIdentifierStr();
	lastNumVal = lexan.getNumVal();
	lastText = lexan.getTextStr();
	getNextToken();
}

shared_ptr<Type> sfeParser::parseType() {
	if (CurTok == tok_kwArray) {
		getNextToken();
		matchToken(tok_bracketLeft);
		/// This would be too hard to generate
		//unique_ptr<Expression> lower = parseExpression();
		unique_ptr<Number> lower = parseNumber();
		matchToken(tok_dot);
		matchToken(tok_dot);
		/// This would be too hard to generate
		//unique_ptr<Expression> upper = parseExpression();
		unique_ptr<Number> upper = parseNumber();
		matchToken(tok_bracketRight);
		matchToken(tok_kwOf);
		return make_shared<Array>(move(lower), move(upper), parseType());

	} else if (CurTok == tok_kwInteger) {
		getNextToken();
		return make_shared<Integer>();
	} else throw InvalidTokException({tok_kwInteger, tok_kwArray}, CurTok);
}

vector<unique_ptr<VarDeclaration>> sfeParser::parseVarDeclaration() {
	vector<string> varNames;
	matchToken(tok_kwVar);
	matchToken(tok_identifier);
	varNames.push_back(lastIdentifier);
	/// OtherVarIdents
	while (CurTok == tok_comma) {
		getNextToken();
		matchToken(tok_identifier);
		varNames.push_back(lastIdentifier);
	}
	matchToken(tok_typeDefine);
	auto type = parseType();
	matchToken(tok_semicolon);
	vector<unique_ptr<VarDeclaration>> ret;
	for (auto name : varNames) {
		ret.push_back(move(make_unique<VarDeclaration>(name, type)));
	}
	/// OtherVarDeclarations
	while (CurTok == tok_identifier) {
		varNames.clear();
		varNames.push_back(lexan.getIdentifierStr());
		getNextToken();
		while (CurTok == tok_comma) {
			matchToken(tok_identifier);
			varNames.push_back(lastIdentifier);
		}
		matchToken(tok_typeDefine);
		type = move(parseType());
		matchToken(tok_semicolon);
		for (auto name : varNames) {
			ret.push_back(move(make_unique<VarDeclaration>(name, move(type))));
		}
	}
	return ret;
}

vector<unique_ptr<ConstDeclaration>> sfeParser::parseConstsDeclaration() {
	vector<unique_ptr<ConstDeclaration>> ret;
	matchToken(tok_kwConst);
	do {
		matchToken(tok_identifier);
		string name = lastIdentifier;
		matchToken(tok_equal);
		matchToken(tok_number);
		ret.push_back(move(make_unique<ConstDeclaration>(name, lastNumVal)));
		matchToken(tok_semicolon);
	} while (CurTok == tok_identifier);
	return ret;
}

vector<unique_ptr<VarDeclaration>> sfeParser::parseParamsDecl() {
	vector<unique_ptr<VarDeclaration>> ret;
	string name;
	if (CurTok == tok_identifier) {
		name = lexan.getIdentifierStr();
		getNextToken();
		matchToken(tok_typeDefine);
		ret.push_back(move(make_unique<VarDeclaration>(name, move(parseType()))));
		while (CurTok == tok_semicolon) {
			getNextToken();
			matchToken(tok_identifier);
			name = lastIdentifier;
			matchToken(tok_typeDefine);
			ret.push_back(move(make_unique<VarDeclaration>(name, move(parseType()))));
		}
	}
	return ret;
}

unique_ptr<FunctionProtoDeclaration> sfeParser::parseFunctionProto() {
	matchToken(tok_kwFunction);
	matchToken(tok_identifier);
	string name = lastIdentifier;
	matchToken(tok_parenthesisLeft);
	auto params = move(parseParamsDecl());
	matchToken(tok_parenthesisRight);
	matchToken(tok_typeDefine);
	auto type = parseType();
	matchToken(tok_semicolon);
	return make_unique<FunctionProtoDeclaration>(name, move(params), type);
}

unique_ptr<ProcedureProtoDeclaration> sfeParser::parseProcedureProto() {
	matchToken(tok_kwProcedure);
	matchToken(tok_identifier);
	string name = lastIdentifier;
	matchToken(tok_parenthesisLeft);
	auto params = move(parseParamsDecl());
	matchToken(tok_parenthesisRight);
	matchToken(tok_semicolon);
	return make_unique<ProcedureProtoDeclaration>(name, move(params));
}

unique_ptr<Body> sfeParser::parseBody() {
	vector<unique_ptr<Statement>> statements;
	if (CurTok == tok_kwBegin) {
		getNextToken();
		if (CurTok == tok_kwEnd) {
			return make_unique<Body>(move(statements));
		}
		statements.push_back(parseStatement());
		while (CurTok == tok_semicolon) {
			getNextToken();
			if (CurTok == tok_kwEnd) break;
			statements.push_back(parseStatement());
		}
		matchToken(tok_kwEnd);
		return make_unique<Body>(move(statements));
	} else {
		statements.push_back(parseStatement());
		return make_unique<Body>(move(statements));
	}
}

unique_ptr<FunctionDeclaration> sfeParser::parseFunction() {
	unique_ptr<FunctionProtoDeclaration> proto = parseFunctionProto();
	vector<unique_ptr<Declaration>> declarations;
	unique_ptr<Body> body = NULL;
	if (CurTok == tok_kwForward){
		getNextToken();
		matchToken(tok_semicolon);
		return make_unique<FunctionDeclaration>(move(proto), move(declarations), move(body));
	}
	while (true) {
		if (CurTok == tok_kwVar) {
			auto res = parseVarDeclaration();
			for (unsigned int i = 0; i < res.size(); i++) {
				declarations.push_back(move(res.at(i)));
			}
		} else if (CurTok == tok_kwConst) {
			auto res = parseConstsDeclaration();
			for (unsigned int i = 0; i < res.size(); i++) {
				declarations.push_back(move(res.at(i)));
			}
		} else {
			body = parseBody();
			break;
		}
	}
	matchToken(tok_semicolon);
	return make_unique<FunctionDeclaration>(move(proto), move(declarations), move(body));
}

unique_ptr<ProcedureDeclaration> sfeParser::parseProcedure() {
	unique_ptr<ProcedureProtoDeclaration> proto = parseProcedureProto();
	vector<unique_ptr<Declaration>> declarations;
	unique_ptr<Body> body = NULL;
	if (CurTok == tok_kwForward){
		getNextToken();
		matchToken(tok_semicolon);
		return make_unique<ProcedureDeclaration>(move(proto), move(declarations), move(body));
	}
	while (true) {
		if (CurTok == tok_kwVar) {
			auto res = parseVarDeclaration();
			for (unsigned int i = 0; i < res.size(); i++) {
				declarations.push_back(move(res.at(i)));
			}
			continue;
		} else if (CurTok == tok_kwConst) {
			auto res = parseConstsDeclaration();
			for (unsigned int i = 0; i < res.size(); i++) {
				declarations.push_back(move(res.at(i)));
			}
			continue;
		} else {
			body = parseBody();
			break;
		}
	}
	matchToken(tok_semicolon);
	return make_unique<ProcedureDeclaration>(move(proto), move(declarations), move(body));
}

unique_ptr<Program> sfeParser::parseProgram() {
	matchToken(tok_kwProgram);
	matchToken(tok_identifier);
	string name = lastIdentifier;
	matchToken(tok_semicolon);
	vector<unique_ptr<Declaration>> declarations;
	unique_ptr<Body> body;
	while (true) {
		if (CurTok == tok_kwVar) {
			auto res = parseVarDeclaration();
			for (unsigned int i = 0; i < res.size(); i++) {
				declarations.push_back(move(res.at(i)));
			}
			continue;
		} else if (CurTok == tok_kwConst) {
			auto res = parseConstsDeclaration();
			for (unsigned int i = 0; i < res.size(); i++) {
				declarations.push_back(move(res.at(i)));
			}
			continue;
		} else if (CurTok == tok_kwFunction) {
			declarations.push_back(move(parseFunction()));
			continue;
		} else if (CurTok == tok_kwProcedure) {
			declarations.push_back(move(parseProcedure()));
			continue;
		} else {
			body = parseBody();
			break;
		}
	}

	matchToken(tok_dot);
	return make_unique<Program>(name, move(declarations), move(body));
}

unique_ptr<Number> sfeParser::parseNumber() {
	if (CurTok == tok_minus) {
		getNextToken();
		matchToken(tok_number);
		return make_unique<Number>(-lastNumVal);
	}
	matchToken(tok_number);
	return make_unique<Number>(lastNumVal);
}

unique_ptr<Exit> sfeParser::parseExit() {
	matchToken(tok_kwExit);
	return make_unique<Exit>();
}

unique_ptr<If> sfeParser::parseIf() {
	matchToken(tok_kwIf);
	unique_ptr<Expression> condition = parseExpression();
	matchToken(tok_kwThen);
	unique_ptr<Body> thenBody = parseBody();
	unique_ptr<Body> elseBody = nullptr;
	if (CurTok == tok_kwElse) {
		getNextToken();
		elseBody = move(parseBody());
	}
	return make_unique<If>(move(condition), move(thenBody), move(elseBody));
}

unique_ptr<While> sfeParser::parseWhile() {
	matchToken(tok_kwWhile);
	unique_ptr<Expression> condition = parseExpression();
	matchToken(tok_kwDo);
	unique_ptr<Body> doBody = parseBody();
	return make_unique<While>(move(condition), move(doBody));
}

unique_ptr<For> sfeParser::parseFor() {
	matchToken(tok_kwFor);
	matchToken(tok_identifier);
	string varName = lastIdentifier;
	matchToken(tok_assign);
	unique_ptr<Expression> initialVal = parseExpression();
	bool ascending;
	if (CurTok == tok_kwTo) {
		ascending = true;
		getNextToken();
	} else if (CurTok == tok_kwDownTo) {
		ascending = false;
		getNextToken();
	} else throw InvalidTokException({tok_kwTo, tok_kwDownTo}, CurTok);
	unique_ptr<Expression> limit = parseExpression();
	unique_ptr<Expression> by = nullptr;
	if (CurTok == tok_kwBy) {
		getNextToken();
		by = parseExpression();
	}
	matchToken(tok_kwDo);
	unique_ptr<Body> doBody = parseBody();
	return make_unique<For>(varName, move(initialVal), move(limit), ascending, move(by), move(doBody));
}

vector<unique_ptr<Expression>> sfeParser::parseCallParams() {
	vector<unique_ptr<Expression>> ret;
	if (CurTok != tok_parenthesisRight) {
		ret.push_back(move(parseExpression()));
	}
	while (CurTok != tok_parenthesisRight) {
		matchToken(tok_comma);
		ret.push_back(move(parseExpression()));
	}
	return ret;
}

unique_ptr<ProcedureCall> sfeParser::parseProcedureCall(string name) {
	matchToken(tok_parenthesisLeft);
	auto params = parseCallParams();
	matchToken(tok_parenthesisRight);
	return make_unique<ProcedureCall>(name, move(params));
}

unique_ptr<FunctionCall> sfeParser::parseFunctionCall(string name) {
	matchToken(tok_parenthesisLeft);
	auto params = parseCallParams();
	matchToken(tok_parenthesisRight);
	return make_unique<FunctionCall>(name, move(params));
}

unique_ptr<ArrayItemReference> sfeParser::parseArrayReference(unique_ptr<VarReference> base) {
	matchToken(tok_bracketLeft);
	unique_ptr<Expression> index = parseExpression();
	matchToken(tok_bracketRight);
	return make_unique<ArrayItemReference>(move(base), move(index));
}

unique_ptr<VarReference> sfeParser::parseVarReference(string name) {
	unique_ptr<VarReference> ret = make_unique<SimpleVarReference>(name);
	while (CurTok == tok_bracketLeft) {
		ret = parseArrayReference(move(ret));
	}
	return ret;
}

unique_ptr<Expression> sfeParser::parseIdentifierExpression() {
	matchToken(tok_identifier);
	string name = lastIdentifier;
	if (CurTok == tok_parenthesisLeft)
		return parseFunctionCall(name);
	else return parseVarReference(name);
}

unique_ptr<Expression> sfeParser::parseString() {
	matchToken(tok_string);
	return make_unique<String>(lastText);
}

unique_ptr<Expression> sfeParser::parseLiteral() {
	if (CurTok == tok_number || CurTok == tok_minus) {
		return parseNumber();
	} else if (CurTok == tok_identifier) {
		return parseIdentifierExpression();
	} else if (CurTok == tok_string) {
		return parseString();
	} else {
		matchToken(tok_parenthesisLeft);
		auto tmp = parseExpression();
		matchToken(tok_parenthesisRight);
		return tmp;
	}
}

unique_ptr<Expression> sfeParser::parseSecondOrderExpression() {
	unique_ptr<Expression> ret = parseLiteral();
	while (1) {
		if (CurTok == tok_multiply) {
			getNextToken();
			ret = make_unique<BinOp>(move(ret), parseLiteral(), bo_multiply);
			continue;
		} else if (CurTok == tok_kwDivide) {
			getNextToken();
			ret = make_unique<BinOp>(move(ret), parseLiteral(), bo_divide);
			continue;
		} else if (CurTok == tok_kwModulo) {
			getNextToken();
			ret = make_unique<BinOp>(move(ret), parseLiteral(), bo_modulo);
			continue;
		} else break;
	}
	return ret;
}

unique_ptr<Expression> sfeParser::parseFirstOrderExpression() {
	unique_ptr<Expression> ret = parseSecondOrderExpression();
	while (1) {
		if (CurTok == tok_plus) {
			getNextToken();
			ret = make_unique<BinOp>(move(ret), parseSecondOrderExpression(), bo_plus);
			continue;
		} else if (CurTok == tok_minus) {
			getNextToken();
			ret = make_unique<BinOp>(move(ret), parseSecondOrderExpression(), bo_minus);
			continue;
		} else break;
	}
	return ret;
}

unique_ptr<Expression> sfeParser::parseExpression() {
	unique_ptr<Expression> ret = parseFirstOrderExpression();
	while (1) {
		if (CurTok == tok_equal) {
			getNextToken();
			ret = make_unique<BinOp>(move(ret), parseFirstOrderExpression(), bo_equal);
			continue;
		} else if (CurTok == tok_nonEqual) {
			getNextToken();
			ret = make_unique<BinOp>(move(ret), parseFirstOrderExpression(), bo_nonEqual);
			continue;
		} else if ((CurTok) == tok_less) {
			getNextToken();
			ret = make_unique<BinOp>(move(ret), parseFirstOrderExpression(), bo_less);
			continue;
		} else if ((CurTok) == tok_lessEqual) {
			getNextToken();
			ret = make_unique<BinOp>(move(ret), parseFirstOrderExpression(), bo_lessEqual);
			continue;
		} else if ((CurTok) == tok_greater) {
			getNextToken();
			ret = make_unique<BinOp>(move(ret), parseFirstOrderExpression(), bo_greater);
			continue;
		} else if ((CurTok) == tok_greaterEqual) {
			getNextToken();
			ret = make_unique<BinOp>(move(ret), parseFirstOrderExpression(), bo_greaterEqual);
			continue;
		} else if ((CurTok) == tok_kwAnd) {
			getNextToken();
			ret = make_unique<BinOp>(move(ret), parseFirstOrderExpression(), bo_and);
			continue;
		} else if ((CurTok) == tok_kwOr) {
			getNextToken();
			ret = make_unique<BinOp>(move(ret), parseFirstOrderExpression(), bo_or);
			continue;
		} else break;
	}
	return ret;
}

unique_ptr<Assign> sfeParser::parseAssign(string name) {
	unique_ptr<VarReference> ref = parseVarReference(name);
	matchToken(tok_assign);
	return make_unique<Assign>(move(ref), parseExpression());
}

unique_ptr<Assign> sfeParser::parseAssign() {
	matchToken(tok_identifier);
	return parseAssign(lastIdentifier);
}

unique_ptr<Break> sfeParser::parseBreak(){
	matchToken(tok_kwBreak);
	return make_unique<Break>();
}

unique_ptr<Statement> sfeParser::parseIdentifierStatement() {
	matchToken(tok_identifier);
	if (CurTok == tok_parenthesisLeft) return parseProcedureCall(lastIdentifier);
	else return parseAssign(lastIdentifier);
}

unique_ptr<Statement> sfeParser::parseStatement() {
	if (CurTok == tok_kwIf) return parseIf();
	else if (CurTok == tok_kwWhile) return parseWhile();
	else if (CurTok == tok_kwFor) return parseFor();
	else if (CurTok == tok_kwExit) return parseExit();
	else if (CurTok == tok_kwBreak) return parseBreak();
	else if (CurTok == tok_identifier) return parseIdentifierStatement();
	else throw InvalidTokException({tok_kwIf, tok_kwWhile, tok_kwFor, tok_kwExit, tok_identifier}, CurTok);
}

sfeParser::sfeParser(std::string filename) : lexan(filename) {
	this->getNextToken();
}
