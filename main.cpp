#include <iostream>
#include "parser.h"
#include "AST.h"

#include <llvm/Support/raw_ostream.h>
#include "llvm/ADT/ArrayRef.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/IRBuilder.h"

using namespace std;

int main(int argc, char* argv[]) {
	string inputFile;
	bool outputFileSet = false;
	string outputFile = "output.o";
	bool debug = false;
	for (int i = 1; i < argc; i++){
		if (strcmp(argv[i], "-o") == 0){
			if (i+1 != argc && !outputFileSet){
				cerr<<"Error: Either you didn't specify output filename after -o or you already set output filename once!"<<endl;
				return 1;
			}
			else{
				outputFileSet = true;
				outputFile = argv[++i];
			}
		}
		else if (strcmp(argv[i],"-d") == 0){
			debug = true;
		}
		else {
			if (inputFile == ""){
				inputFile = argv[i];
			} else {
				cerr<<"Error: Wrong arguments!"<<endl;
				return 1;
			}
		}
	}
	if (inputFile == ""){
		cerr<<"Error: No input filename specified!"<<endl;
		return 1;
	}
	if (outputFileSet && debug){
		cerr<<"Warning: There was no reason to specify output filename when debugging, no file will be made!"<<endl;
	}
	sfeParser parser(inputFile);
	try {
		auto res = parser.parseProgram();
		res->getLLVM();
		if (debug){
			res->printModule();
		}
		else res->saveToObjectFile(outputFile);
	} catch (InvalidTokException & e){
		cout<<e.message()<<endl;
		return 1;
	} catch (const char * e){
		cout<<e<<endl;
		return 1;
	}
	return 0;
}