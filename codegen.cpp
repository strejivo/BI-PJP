#include "llvm/ADT/APFloat.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Verifier.h"
#include "llvm/IR/Instructions.h"
#include <llvm/ExecutionEngine/ExecutionEngine.h>
#include <llvm/ExecutionEngine/GenericValue.h>
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/Host.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/TargetRegistry.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Target/TargetOptions.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/ADT/IndexedMap.h"
#include <iostream>

#include "AST.h"


using namespace std;
static llvm::LLVMContext TheContext;
static llvm::IRBuilder<> Builder(TheContext);
static unique_ptr<llvm::Module> TheModule;
static llvm::Value *globalNumberPrintFormat;
static llvm::Value *globalPrintfNewLineFormat;
/// First map is for context (as in global, func etc.), the second is for the name it self
/// Example for global foo: NamedValues["global"]["foo"] NOTE that this assumes that it exists
static map<string, map<string, llvm::AllocaInst *>> NamedValues;
static map<string, map<string, llvm::Value *>> ConstValues;
static map<string, llvm::GlobalVariable *> GlobalValues;
static map<string, llvm::Value *> GlobalConstValues;

static map<string, llvm::Value *> GlobalArrayLowerBound;
static map<string, map<string, llvm::Value *>> ArrayLowerBound;

static llvm::BasicBlock * whereToBreak;

static bool alreadyBreakedInBlock = false;
static bool alreadyExitedInBlock = false;


/// CreateEntryBlockAlloca - Create an alloca instruction in the entry block of
/// the function.  This is used for mutable variables etc.
static llvm::AllocaInst *CreateEntryBlockAlloca(llvm::Function *TheFunction,
										  const std::string &VarName, llvm::Type * type) {
	llvm::IRBuilder<> TmpB(&TheFunction->getEntryBlock(),
					 TheFunction->getEntryBlock().begin());
	return TmpB.CreateAlloca(type, llvm::ConstantInt::get(llvm::Type::getInt32Ty(TheContext), (type->isArrayTy() ? type->getArrayNumElements() : 0), false),
							 VarName.c_str());
}

llvm::Type *sfeAST::Integer::getLLVMType() {
	return llvm::Type::getInt32Ty(TheContext);
}

llvm::Constant *sfeAST::Integer::getInitValue() {
	return llvm::ConstantInt::get(TheContext, llvm::APInt(32, 0, true));
}


llvm::Type *sfeAST::Array::getLLVMType() {
	llvm::Type *innerType = this->type->getLLVMType();
	return llvm::ArrayType::get(innerType, this->maxIndex->val - this->minIndex->val + 1);
}

llvm::Constant *sfeAST::Array::getInitValue() {
	return llvm::ConstantArray::get((llvm::ArrayType *)this->getLLVMType(), this->type->getInitValue());
}

void sfeAST::VarDeclaration::translateToLLVM(bool isGlobal) {
	auto type = this->type->getLLVMType();
	string fName = Builder.GetInsertBlock()->getParent()->getName();
	if (isGlobal) {
		if (type->isArrayTy()) {
			int lowerBound = ((Array *)this->type.get())->minIndex->val;
			GlobalArrayLowerBound[this->identifier] = llvm::ConstantInt::get(llvm::Type::getInt32Ty(TheContext), lowerBound, true);
			TheModule->getOrInsertGlobal(this->identifier, type);
			llvm::GlobalVariable *gVar = TheModule->getNamedGlobal(this->identifier);
			gVar->setLinkage(llvm::GlobalValue::CommonLinkage);
			gVar->setAlignment(16);
			gVar->setInitializer(this->type->getInitValue());
			GlobalValues.insert(make_pair(this->identifier, gVar));

		}
		else {
			TheModule->getOrInsertGlobal(this->identifier, type);
			llvm::GlobalVariable *gVar = TheModule->getNamedGlobal(this->identifier);
			gVar->setLinkage(llvm::GlobalValue::CommonLinkage);
			gVar->setAlignment(4);
			gVar->setInitializer(this->type->getInitValue());
			GlobalValues.insert(make_pair(this->identifier, gVar));
		}
	}
	else {
		if (type->isArrayTy()){
			int lowerBound = ((Array *)this->type.get())->minIndex->val;
			ArrayLowerBound[fName][this->identifier] = llvm::ConstantInt::get(llvm::Type::getInt32Ty(TheContext), lowerBound, true);
			NamedValues[fName][this->identifier] = Builder.CreateAlloca(type, NULL, this->identifier);
		}
		else NamedValues[fName][this->identifier] = Builder.CreateAlloca(type, NULL, this->identifier);
	}
}

void sfeAST::ConstDeclaration::translateToLLVM(bool isGlobal) {
	auto tmp = llvm::ConstantInt::get(TheContext, llvm::APInt(32, this->value, true));
	if (isGlobal){
		GlobalConstValues.insert(make_pair(this->identifier, tmp));
	}
	else ConstValues[Builder.GetInsertBlock()->getParent()->getName()][this->identifier] = tmp;
}


llvm::Function *sfeAST::FunctionProtoDeclaration::codegen() {
	std::vector<llvm::Type *> params;
	for (int i = 0; i < this->params.size(); i++) {
		params.push_back(this->params[i]->type->getLLVMType());
	}
	llvm::FunctionType *FT =
	  llvm::FunctionType::get(this->retType->getLLVMType(), params, false);

	llvm::Function *F =
	  llvm::Function::Create(FT, llvm::Function::ExternalLinkage, this->name, TheModule.get());

	// Set names for all arguments.
	unsigned Idx = 0;
	for (auto &Arg : F->args())
		Arg.setName(this->params[Idx++]->identifier);

	return F;
}

llvm::Function *sfeAST::ProcedureProtoDeclaration::codegen() {
	std::vector<llvm::Type *> params;
	for (int i = 0; i < this->params.size(); i++) {
		params.push_back(this->params[i]->type->getLLVMType());
	}
	llvm::FunctionType *FT =
	  llvm::FunctionType::get(llvm::Type::getVoidTy(TheContext), params, false);

	llvm::Function *F =
	  llvm::Function::Create(FT, llvm::Function::ExternalLinkage, this->name, TheModule.get());

	// Set names for all arguments.
	unsigned Idx = 0;
	for (auto &Arg : F->args())
		Arg.setName(this->params[Idx++]->identifier);

	return F;
}


llvm::Value *sfeAST::Body::codegen() {
	for (auto &it : statements)
		it->translateToLLVM();
}

void sfeAST::FunctionDeclaration::translateToLLVM(bool isGlobal) {
	llvm::Function *TheFunction = TheModule->getFunction(this->proto->name);
	if (!TheFunction)
		TheFunction = this->proto->codegen();

	if (!TheFunction)
		return;
	if (!this->body) return;
	// Create a new basic block to start insertion into.
	llvm::BasicBlock *BB = llvm::BasicBlock::Create(TheContext, "procedure"+proto->name+"block", TheFunction);
	auto prevBB = Builder.GetInsertBlock();
	Builder.SetInsertPoint(BB);

	NamedValues[this->proto->name][this->proto->name] = CreateEntryBlockAlloca(TheFunction, this->proto->name, this->proto->retType->getLLVMType());
	for (auto &Arg : TheFunction->args()) {
		// Create an alloca for this variable.
		llvm::AllocaInst *Alloca = CreateEntryBlockAlloca(TheFunction, Arg.getName(), Arg.getType());
		// Store the initial value into the alloca.
		Builder.CreateStore(&Arg, Alloca);

		// Add arguments to variable symbol table.
		NamedValues[this->proto->name][Arg.getName()] = Alloca;
	}

	for (auto &it : declarations) {
		it->translateToLLVM(true);
	}
	alreadyExitedInBlock = false;
	this->body->codegen();
	if (!alreadyExitedInBlock)
		Builder.CreateRet(Builder.CreateLoad(NamedValues[this->proto->name][this->proto->name]));
	Builder.SetInsertPoint(prevBB);
}

void sfeAST::ProcedureDeclaration::translateToLLVM(bool isGlobal) {
	llvm::Function *TheFunction = TheModule->getFunction(this->proto->name);

	if (!TheFunction)
		TheFunction = this->proto->codegen();

	if (!TheFunction)
		return;
	if (!this->body) return;
	// Create a new basic block to start insertion into.
	llvm::BasicBlock *BB = llvm::BasicBlock::Create(TheContext, "procedure"+proto->name+"block", TheFunction);
	auto prevBB = Builder.GetInsertBlock();
	Builder.SetInsertPoint(BB);
	for (auto &Arg : TheFunction->args()) {
		// Create an alloca for this variable.
		llvm::AllocaInst *Alloca = CreateEntryBlockAlloca(TheFunction, Arg.getName(), Arg.getType());
		// Store the initial value into the alloca.
		Builder.CreateStore(&Arg, Alloca);

		// Add arguments to variable symbol table.
		NamedValues[this->proto->name][Arg.getName()] = Alloca;
	}
	for (auto &it : declarations) {
		it->translateToLLVM();
	}
	this->body->codegen();
	Builder.CreateRetVoid();
	Builder.SetInsertPoint(prevBB);
}

void sfeAST::Program::translateToLLVM() {
	llvm::Function *mprintf = llvm::Function::Create(llvm::FunctionType::get(
	  llvm::Type::getInt32Ty(TheContext), {llvm::Type::getInt8PtrTy(TheContext)}, true),
													 llvm::Function::ExternalLinkage, llvm::Twine("printf"),
													 TheModule.get());
	mprintf->setCallingConv(llvm::CallingConv::C);

	llvm::Function *mscanf = llvm::Function::Create(llvm::FunctionType::get(
			llvm::Type::getInt32Ty(TheContext), {llvm::Type::getInt8PtrTy(TheContext)}, true), llvm::Function::ExternalLinkage,
													llvm::Twine("scanf"),
													TheModule.get()
			);
	mscanf->setCallingConv(llvm::CallingConv::C);

	llvm::Function *main = llvm::Function::Create(
	  llvm::FunctionType::get(llvm::IntegerType::getInt32Ty(TheContext), {}, false),
	  llvm::GlobalValue::ExternalLinkage, "main", TheModule.get());
	main->setCallingConv(llvm::CallingConv::C);
	llvm::BasicBlock *mainBlock = llvm::BasicBlock::Create(TheContext, "mainBlock", main);
	Builder.SetInsertPoint(mainBlock);
	globalNumberPrintFormat = Builder.CreateGlobalStringPtr("%d");
	globalPrintfNewLineFormat = Builder.CreateGlobalStringPtr("\n");
	for (auto &it : declarations) {
		it->translateToLLVM(true);
	}
	this->body->codegen();
	Builder.CreateRet(llvm::ConstantInt::get(TheContext, llvm::APInt(32, 0)));
}
void sfeAST::Program::getLLVM() {
	TheModule = llvm::make_unique<llvm::Module>("", TheContext);
	this->translateToLLVM();
}

void sfeAST::Program::saveToObjectFile(const string &filename) {
	llvm::InitializeAllTargetInfos();
	llvm::InitializeAllTargets();
	llvm::InitializeAllTargetMCs();
	llvm::InitializeAllAsmParsers();
	llvm::InitializeAllAsmPrinters();

	auto TargetTriple = llvm::sys::getDefaultTargetTriple();
	TheModule->setTargetTriple(TargetTriple);

	std::string Error;
	auto Target = llvm::TargetRegistry::lookupTarget(TargetTriple, Error);

	// Print an error and exit if we couldn't find the requested target.
	// This generally occurs if we've forgotten to initialise the
	// TargetRegistry or we have a bogus target triple.
	if (!Target) {
		cerr << Error <<endl;
		return;
	}

	auto CPU = "generic";
	auto Features = "";

	llvm::TargetOptions opt;
	auto RM = llvm::Optional<llvm::Reloc::Model>();
	auto TheTargetMachine =
	  Target->createTargetMachine(TargetTriple, CPU, Features, opt, RM);

	TheModule->setDataLayout(TheTargetMachine->createDataLayout());

	std::error_code EC;
	llvm::raw_fd_ostream dest(filename, EC, llvm::sys::fs::F_None);

	if (EC) {
		llvm::errs() << "Could not open file: " << EC.message();
		return;
	}

	llvm::legacy::PassManager pass;
	auto FileType = llvm::TargetMachine::CGFT_ObjectFile;

	if (TheTargetMachine->addPassesToEmitFile(pass, dest, FileType)) {
		llvm::errs() << "TheTargetMachine can't emit a file of this type";
		return;
	}

	pass.run(*TheModule);
	dest.flush();
}

void sfeAST::Program::printModule() {
	TheModule->print(llvm::outs(), nullptr);
}

llvm::Value *sfeAST::ArrayItemReference::getLLVMValue() {
	return Builder.CreateLoad(this->getLLVMAddress());
}

llvm::Value *sfeAST::ArrayItemReference::getLLVMAddress() {
	string arrayName = ((SimpleVarReference *)this->var.get())->name;
	string fName = Builder.GetInsertBlock()->getParent()->getName();
	if (GlobalValues.find(arrayName) != GlobalValues.end()){
		vector<llvm::Value *> inx;
		inx.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(TheContext), 0));
		inx.push_back(Builder.CreateSub(index->getLLVMValue(), GlobalArrayLowerBound[arrayName]));
		return Builder.CreateGEP(GlobalValues[arrayName], inx);
	}
	else if (NamedValues[fName].find(arrayName) != NamedValues[fName].end()){
		vector<llvm::Value *> inx;
		inx.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(TheContext), 0));
		inx.push_back(Builder.CreateSub(index->getLLVMValue(), ArrayLowerBound[fName][arrayName]));
		return Builder.CreateGEP(NamedValues[fName].at(arrayName), inx);
	}
}

llvm::Value *sfeAST::SimpleVarReference::getLLVMValue() {
	string fName = Builder.GetInsertBlock()->getParent()->getName();
	if (ConstValues[fName].find(this->name) != ConstValues[fName].end()){
		return ConstValues[fName].at(this->name);
	}
	if (NamedValues[fName].find(this->name) != NamedValues[fName].end()){
		return Builder.CreateLoad(NamedValues[fName].at(this->name), this->name);
	}
	if (GlobalConstValues.find(this->name) != GlobalConstValues.end()){
		return GlobalConstValues.at(this->name);
	}
	if (GlobalValues.find(this->name) != GlobalValues.end()){
		return Builder.CreateLoad(GlobalValues.at(this->name), this->name);
	}
	throw "Unknown var";
}

llvm::Value * sfeAST::SimpleVarReference::getLLVMAddress() {
	string fName = Builder.GetInsertBlock()->getParent()->getName();
	if (NamedValues[fName].find(this->name) != NamedValues[fName].end()){
		return NamedValues[fName].at(this->name);
	}
	if (GlobalValues.find(this->name) != GlobalValues.end()){
		return GlobalValues.at(this->name);
	}
	throw "Either const or unknown var";
}


void sfeAST::Assign::translateToLLVM() {
	llvm::Value * var_ref = this->var->getLLVMAddress();
	Builder.CreateStore(this->value->getLLVMValue(), var_ref);
	alreadyBreakedInBlock = false;
}

//Working
void sfeAST::If::translateToLLVM() {
	llvm::Value * cond = condition->getLLVMValue();
	cond = Builder.CreateICmpNE(cond , llvm::ConstantInt::get(TheContext, llvm::APInt(32, 0, true)));
	llvm::Function *f = Builder.GetInsertBlock()->getParent();
	llvm::BasicBlock *ThenBB = llvm::BasicBlock::Create(TheContext, "then", f);
	llvm::BasicBlock *ElseBB = llvm::BasicBlock::Create(TheContext, "else");
	llvm::BasicBlock *MergeBB = llvm::BasicBlock::Create(TheContext, "mergeIf");
	Builder.CreateCondBr(cond, ThenBB, ElseBB);
	Builder.SetInsertPoint(ThenBB);
	alreadyBreakedInBlock= false;
	then->codegen();
	if (!alreadyBreakedInBlock) Builder.CreateBr(MergeBB);

	f->getBasicBlockList().push_back(ElseBB);
	Builder.SetInsertPoint(ElseBB);
	alreadyBreakedInBlock = false;
	if (elseBody)
		elseBody->codegen();
	if (!alreadyBreakedInBlock)
		Builder.CreateBr(MergeBB);
	f->getBasicBlockList().push_back(MergeBB);
	Builder.SetInsertPoint(MergeBB);
	alreadyBreakedInBlock = false;
	alreadyExitedInBlock = false;
}

void sfeAST::For::translateToLLVM() {
	llvm::Function *f = Builder.GetInsertBlock()->getParent();
	string fName = f->getName();
	// Create an alloca for the variable in the entry block.
	llvm::AllocaInst *Alloca = CreateEntryBlockAlloca(f, this->varName, llvm::Type::getInt32Ty(TheContext));

	// Emit the start code first, without 'variable' in scope.
	llvm::Value *StartVal = this->startValue->getLLVMValue();
	if (!StartVal) {
		return;
	}

	// Store the value into the alloca.
	Builder.CreateStore(StartVal, Alloca);

	// Make the new basic block for the loop header, inserting after current
	// block.
	llvm::BasicBlock *LoopBB = llvm::BasicBlock::Create(TheContext, "forLoop", f);
	llvm::BasicBlock *AfterLoopBB = llvm::BasicBlock::Create(TheContext, "afterForLoop");
	// Insert an explicit fall through from the current block to the LoopBB.
	Builder.CreateBr(LoopBB);

	// Start insertion in LoopBB.
	Builder.SetInsertPoint(LoopBB);

	llvm::AllocaInst *OldVal = NamedValues[fName][varName];
	NamedValues[fName][varName] = Alloca;

	llvm::BasicBlock * oldBreakPoint = whereToBreak;
	whereToBreak = AfterLoopBB;
	this->body->codegen();

	whereToBreak = oldBreakPoint;
	// Emit the step value.
	llvm::Value *StepVal = nullptr;
	if (this->by) {
		StepVal = this->by->getLLVMValue();
		if (!StepVal)
			return;
	} else {
		// If not specified, use 1.0.
		StepVal = llvm::ConstantInt::get(TheContext, llvm::APInt(32, 1, true));
	}

	// Compute the end condition.
	llvm::Value *EndCond = this->endValue->getLLVMValue();
	if (!EndCond)
		return;

	// Reload, increment, and restore the alloca.  This handles the case where
	// the body of the loop mutates the variable.
	llvm::Value *CurVar = Builder.CreateLoad(Alloca, varName.c_str());
	llvm::Value *NextVar;
	if (ascending)
	NextVar = Builder.CreateAdd(CurVar, StepVal, "nextvar");
	else NextVar = Builder.CreateSub(CurVar, StepVal, "nextvar");
	Builder.CreateStore(NextVar, Alloca);

	// Convert condition to a bool by comparing non-equal to 0.0.
	EndCond = Builder.CreateICmpNE(EndCond , CurVar);


	// Insert the conditional branch into the end of LoopEndBB.
	Builder.CreateCondBr(EndCond, LoopBB, AfterLoopBB);

	f->getBasicBlockList().push_back(AfterLoopBB);

	Builder.SetInsertPoint(AfterLoopBB);
	alreadyBreakedInBlock = false;
	alreadyExitedInBlock = false;
	// Restore the unshadowed variable.
	if (OldVal)
		NamedValues[fName][varName] = OldVal;
	else
		NamedValues.erase(varName);
}

void sfeAST::While::translateToLLVM() {
	llvm::Function* f = Builder.GetInsertBlock()->getParent();
	llvm::BasicBlock* loopBB = llvm::BasicBlock::Create(TheContext, "whileLoop");
	llvm::BasicBlock* loopCondBB = llvm::BasicBlock::Create(TheContext, "whileCond", f);
	llvm::BasicBlock* AfterLoopBB = llvm::BasicBlock::Create(TheContext, "afterWhileLoop");
	Builder.CreateBr(loopCondBB);
	Builder.SetInsertPoint(loopCondBB);
	llvm::Value* cond = condition->getLLVMValue();
	cond = Builder.CreateICmpNE(cond, llvm::ConstantInt::get(TheContext, llvm::APInt(32, 0, true)));
	Builder.CreateCondBr(cond, loopBB, AfterLoopBB);
	f->getBasicBlockList().push_back(loopBB);
	Builder.SetInsertPoint(loopBB);
	llvm::BasicBlock *oldBreakPoint = whereToBreak;
	whereToBreak = AfterLoopBB;
	alreadyBreakedInBlock = false;
	this->body->codegen();
	whereToBreak = oldBreakPoint;
	if (!alreadyBreakedInBlock) Builder.CreateBr(loopCondBB);
	f->getBasicBlockList().push_back(AfterLoopBB);
	Builder.SetInsertPoint(AfterLoopBB);
	alreadyBreakedInBlock = false;
	alreadyExitedInBlock = false;
}

void sfeAST::ProcedureCall::translateToLLVM() {
	if (name == "write" || name == "writeln") {
		auto val = parameters[0]->getLLVMValue();
		if (val->getType() == llvm::Type::getInt32Ty(TheContext)) {
			Builder.CreateCall(TheModule->getFunction("printf"), {globalNumberPrintFormat, val});
			if (name == "writeln") Builder.CreateCall(TheModule->getFunction("printf"), {globalPrintfNewLineFormat});
		}
		else {
			llvm::GlobalVariable *var =
			  new llvm::GlobalVariable(
				*TheModule, llvm::ArrayType::get(llvm::IntegerType::get(TheContext, 8), val->getType()->getArrayNumElements()),
				true, llvm::GlobalValue::PrivateLinkage, (llvm::Constant*)val, ".str");
			llvm::Constant *zero =
			  llvm::Constant::getNullValue(llvm::IntegerType::getInt32Ty(TheContext));

			std::vector<llvm::Constant*> indices;
			indices.push_back(zero);
			indices.push_back(zero);
			llvm::Constant *var_ref =
			  llvm::ConstantExpr::getGetElementPtr(var->getValueType(), var, indices);
			Builder.CreateCall(TheModule->getFunction("printf"), {var_ref});
			if (name == "writeln") Builder.CreateCall(TheModule->getFunction("printf"), {globalPrintfNewLineFormat});
		}
	}
	else if (name == "readln"){
		llvm::Value * paramAddress = ((VarReference *) (parameters[0].get()))->getLLVMAddress();
		Builder.CreateCall(TheModule->getFunction("scanf"), {globalNumberPrintFormat, paramAddress});
	}
	else if (name == "dec"){
		llvm::Value * paramAddress = ((VarReference *) (parameters[0].get()))->getLLVMAddress();
		llvm::Value * currVal = parameters[0]->getLLVMValue();
		Builder.CreateStore(Builder.CreateSub(currVal, llvm::ConstantInt::get(llvm::Type::getInt32Ty(TheContext), 1, true)), paramAddress);
	}
	else {
		llvm::Function *f = TheModule->getFunction(name);
		if (!f) return;
		if (f->arg_size() != parameters.size()) return;
		vector<llvm::Value *> ArgsV;
		for (size_t i = 0; i < parameters.size(); i++)
			ArgsV.push_back(parameters[i]->getLLVMValue());
		Builder.CreateCall(f, ArgsV);

	}
	alreadyBreakedInBlock = false;
	alreadyExitedInBlock = false;
}

llvm::Value *sfeAST::FunctionCall::getLLVMValue() {
	llvm::Function *f = TheModule->getFunction(name);
	if (!f) return NULL;
	if (f->arg_size() != parameters.size()) return NULL;
	vector<llvm::Value *> ArgsV;
	for (size_t i = 0; i < parameters.size(); i++)
		ArgsV.push_back(parameters[i]->getLLVMValue());

	return Builder.CreateCall(f, ArgsV);
}

llvm::Value *sfeAST::BinOp::getLLVMValue() {
	llvm::Value *l = left->getLLVMValue(), *r = right->getLLVMValue();
	if (!l || !r) return NULL;
	llvm::Value * retTmp = NULL;
	switch (op) {
		case bo_plus:
			retTmp = Builder.CreateAdd(l, r);
			break;
		case bo_minus:
			retTmp = Builder.CreateSub(l, r);
			break;
		case bo_divide:
			retTmp = Builder.CreateSDiv(l, r);
			break;
		case bo_multiply:
			retTmp = Builder.CreateMul(l, r);
			break;
		case bo_modulo:
			retTmp = Builder.CreateSRem(l, r);
			break;
		case bo_equal:
			retTmp = Builder.CreateICmpEQ(l, r);
			break;
		case bo_nonEqual:
			retTmp = Builder.CreateICmpNE(l, r);
			break;
		case bo_less:
			retTmp = Builder.CreateICmpSLT(l, r);
			break;
		case bo_lessEqual:
			retTmp = Builder.CreateICmpSLE(l, r);
			break;
		case bo_greater:
			retTmp = Builder.CreateICmpSGT(l, r);
			break;
		case bo_greaterEqual:
			retTmp = Builder.CreateICmpSGE(l, r);
			break;
		case bo_and:
			retTmp = Builder.CreateAnd(l, r);
			break;
		case bo_or:
			retTmp = Builder.CreateOr(l, r);
			break;
	}
	//Make sure it is all 32bit, as comparison returns 1bit int
	return Builder.CreateIntCast(retTmp, llvm::Type::getInt32Ty(TheContext), true);
}

llvm::Value *sfeAST::Number::getLLVMValue() {
	return llvm::ConstantInt::get(TheContext, llvm::APInt(32, val, true));
}

llvm::Value *sfeAST::String::getLLVMValue() {
	return llvm::ConstantDataArray::getString(TheContext, this->text);
}

void sfeAST::Exit::translateToLLVM() {
	llvm::Function * f = Builder.GetInsertBlock()->getParent();
	if (f->getReturnType() != llvm::Type::getVoidTy(TheContext)) {
		string fName = Builder.GetInsertBlock()->getParent()->getName();
		Builder.CreateRet(Builder.CreateLoad(NamedValues[fName][fName]));
	}
	else Builder.CreateRetVoid();
	alreadyBreakedInBlock = true;
	alreadyExitedInBlock = true;
}


void sfeAST::Break::translateToLLVM() {
	Builder.CreateBr(whereToBreak);
	alreadyBreakedInBlock = true;
	alreadyExitedInBlock = false;
}

