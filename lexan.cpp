#include <iostream>
#include "lexan.h"

std::string sfeLexan::getIdentifierStr(){
	return IdentifierStr;
}

int sfeLexan::getNumVal(){
	return NumVal;
}

std::string sfeLexan::getTextStr(){
	return TextStr;
}

Token sfeLexan::gettok()  {
	static int LastChar = ' ';

	// Skip any whitespace.
	while (isspace(LastChar))
		LastChar = input.get();

	if (isalpha(LastChar)) { // identifier: [a-zA-Z][a-zA-Z0-9\_]*
		IdentifierStr = LastChar;
		while (isalnum((LastChar = input.get())) || LastChar == '_')
			IdentifierStr += LastChar;
		auto it = keyWords.find(IdentifierStr);
		if (it != keyWords.end()) {
			return it->second;
		}
		return tok_identifier;
	}

	if (isdigit(LastChar)) { // Decimal number: [0-9]+
		std::string NumStr;
		do {
			NumStr += LastChar;
			LastChar = input.get();
		} while (isdigit(LastChar));

		NumVal = strtol(NumStr.c_str(), nullptr, 10);
		return tok_number;
	}

	if (LastChar == '&') { // Octal number: &[0-7]+
		std::string NumStr;
		LastChar = input.get();
		while (isdigit(LastChar) && LastChar < '8') {
			NumStr += LastChar;
			LastChar = input.get();
		}
		NumVal = strtol(NumStr.c_str(), nullptr, 8);
		return tok_number;
	}

	if (LastChar == '$') { // Hex number: $[0-9]+
		std::string NumStr;
		LastChar = input.get();
		while (isxdigit(LastChar)) {
			NumStr += LastChar;
			LastChar = input.get();
		}
		NumVal = strtol(NumStr.c_str(), nullptr, 16);
		return tok_number;
	}

	if (LastChar == '#') {
		// Comment until end of line.
		do
			LastChar = input.get();
		while (LastChar != EOF && LastChar != '\n' && LastChar != '\r');

		if (LastChar != EOF)
			return gettok();
		else return tok_eof;
	}

	if (LastChar == '\'') {
		TextStr = "";
		while ((LastChar = input.get()) != '\'') {
			if (LastChar == EOF) return tok_eof;
			if (LastChar == '\\') {
				if ((LastChar = input.get()) == '\'') {
					TextStr += '\'';
					continue;
				} else TextStr += '\\';
			}
			TextStr += LastChar;
		}
		LastChar = input.get();
		return tok_string;
	}

	if (LastChar == '/') {
		if ((LastChar = input.get()) == '*') {
			bool end = false;
			do {
				LastChar = input.get();
				if (!end) {
					if (LastChar == '*') end = true;
				}
				else
					if (LastChar == '/') break;
					else end = false;
			}
			while (LastChar != EOF);
			if (LastChar == EOF) return tok_eof;
			LastChar = input.get();
			return gettok();
		}
		else return tok_error;
	}

	Token returnTok = tok_error;
	if (LastChar == '+') returnTok = tok_plus;
	else if (LastChar == '-') returnTok = tok_minus;
	else if (LastChar == '*') returnTok = tok_multiply;

	else if (LastChar == '=') returnTok = tok_equal;
	else if (LastChar == ':') {
		LastChar = input.get();
		if (LastChar == '=') returnTok = tok_assign;
		else return tok_typeDefine;
	} else if (LastChar == '<') {
		LastChar = input.get();
		if (LastChar == '=') returnTok = tok_lessEqual;
		else if (LastChar == '>') returnTok = tok_nonEqual;
		else return tok_less;
	} else if (LastChar == '>') {
		LastChar = input.get();
		if (LastChar == '=') returnTok = tok_greaterEqual;
		else return tok_greater;
	} else if (LastChar == '(') returnTok = tok_parenthesisLeft;
	else if (LastChar == ')') returnTok = tok_parenthesisRight;
	else if (LastChar == '[') returnTok = tok_bracketLeft;
	else if (LastChar == ']') returnTok = tok_bracketRight;
	else if (LastChar == ',') returnTok = tok_comma;
	else if (LastChar == ';') returnTok = tok_semicolon;
	else if (LastChar == '.') returnTok = tok_dot;

	// EOF
	if (LastChar == EOF)
		return tok_eof;
	LastChar = input.get();
	return returnTok;
}

sfeLexan::sfeLexan(std::string filename) {
	input = std::ifstream(filename);
}
