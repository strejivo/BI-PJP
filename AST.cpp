#include <memory>
#include <vector>
#include "AST.h"


using namespace std;

sfeAST::Array::Array(unique_ptr<Number> min, unique_ptr<Number> max, shared_ptr<Type> type) :
  minIndex(move(min)), maxIndex(move(max)), type(type) {}

sfeAST::VarDeclaration::VarDeclaration(const string &name, shared_ptr<sfeAST::Type> type) : identifier(name), type(type) {}

sfeAST::ConstDeclaration::ConstDeclaration(const string &name, int val) : identifier(name), value(val) {}

sfeAST::FunctionProtoDeclaration::FunctionProtoDeclaration(const string &name, vector<unique_ptr<sfeAST::VarDeclaration>> params,
									 shared_ptr<sfeAST::Type> retType) :
  name(name), params(move(params)), retType(retType) {}

sfeAST::ProcedureProtoDeclaration::ProcedureProtoDeclaration(const string &name, vector<unique_ptr<sfeAST::VarDeclaration>> params) :
  name(name), params(move(params)) {}


sfeAST::Body::Body(vector<unique_ptr<sfeAST::Statement>> statements) : statements(move(statements)) {}

sfeAST::FunctionDeclaration::FunctionDeclaration(unique_ptr<sfeAST::FunctionProtoDeclaration> proto, vector<unique_ptr<Declaration>> declarations, unique_ptr<sfeAST::Body> body) :
  proto(move(proto)), declarations(move(declarations)), body(move(body)) {}

sfeAST::ProcedureDeclaration::ProcedureDeclaration(unique_ptr<sfeAST::ProcedureProtoDeclaration> proto, vector<unique_ptr<Declaration>> declarations, unique_ptr<sfeAST::Body> body) :
  proto(move(proto)), declarations(move(declarations)), body(move(body)) {}

sfeAST::Program::Program(const string &name, vector<unique_ptr<Declaration>> declarations, unique_ptr<sfeAST::Body> body) :
  name(name), declarations(move(declarations)), body(move(body)) {}

sfeAST::SimpleVarReference::SimpleVarReference(const string &name) : name(name) {}

sfeAST::ArrayItemReference::ArrayItemReference(unique_ptr<sfeAST::VarReference> var,
											   unique_ptr<sfeAST::Expression> index) :
  var(move(var)), index(move(index)) {}

sfeAST::Assign::Assign(unique_ptr<sfeAST::VarReference> var, unique_ptr<sfeAST::Expression> value) :
  var(move(var)), value(move(value)) {}

sfeAST::If::If(unique_ptr<sfeAST::Expression> condition, unique_ptr<sfeAST::Body> then,
			   unique_ptr<sfeAST::Body> elseBody) :
  condition(move(condition)), then(move(then)), elseBody(move(elseBody)) {}

sfeAST::For::For(const string &varName, unique_ptr<sfeAST::Expression> startValue,
				 unique_ptr<sfeAST::Expression> endValue, bool ascending, unique_ptr<sfeAST::Expression> by,
				 unique_ptr<sfeAST::Body> body) : varName(varName), startValue(move(startValue)),
												  endValue(move(endValue)),
												  ascending(ascending), by(move(by)), body(move(body)) {}

sfeAST::While::While(unique_ptr<sfeAST::Expression> condition, unique_ptr<sfeAST::Body> doBody) : condition(
  move(condition)), body(move(doBody)) {}

sfeAST::ProcedureCall::ProcedureCall(const string &name, vector<unique_ptr<sfeAST::Expression>> parameters) : name(
  name), parameters(move(parameters)) {}

sfeAST::FunctionCall::FunctionCall(const string &name, vector<unique_ptr<sfeAST::Expression>> parameters) : name(name),
																											parameters(
																											  move(
																												parameters)) {}

sfeAST::BinOp::BinOp(unique_ptr<sfeAST::Expression> left, unique_ptr<sfeAST::Expression> right,
					 sfeAST::binOperation op) : left(move(left)), right(move(right)), op(op) {}

sfeAST::Number::Number(int val) : val(val) {}

sfeAST::String::String(const string text) : text(text) {}
