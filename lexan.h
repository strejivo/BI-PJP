#ifndef SFE_LEXAN_H
#define SFE_LEXAN_H

#include <map>
#include <fstream>

enum Token {
	tok_eof, // EOF
	tok_kwProgram,             	// program
	tok_kwBegin,               	// begin
	tok_kwEnd,                 	// end
	tok_kwConst,               	// const
	tok_kwVar,                 	// var
	tok_kwArray,               	// array
	tok_kwFor,                 	// for
	tok_kwDo,					// do
	tok_kwTo,                 	// to
	tok_kwDownTo,             	// downTo
	tok_kwIf,                 	// if
	tok_kwOr,                 	// or
	tok_kwAnd,                	// and
	tok_kwThen,               	// then
	tok_kwElse,               	// else
	tok_kwOf,                 	// of
	tok_kwFunction,           	// function
	tok_kwProcedure,            // procedure
	tok_kwForward,            	// forward
	tok_kwInteger,            	// integer
	tok_kwBy,                   // by
	tok_kwExit,                	// exit
	tok_kwWhile,                // while
	tok_kwString,				// string
	tok_kwDivide,             	// div
	tok_kwModulo,             	// mod
	tok_plus,                 // +
	tok_minus,                	// -
	tok_multiply,             	// *
	tok_equal,                	// =
	tok_assign,               	// :=
	tok_typeDefine,           	// :
	tok_less,                 	// <
	tok_lessEqual,            	// <=
	tok_greater,              	// >
	tok_greaterEqual,         	// >=
	tok_nonEqual,             	// <>
	tok_parenthesisLeft,      	// (
	tok_parenthesisRight,     	// )
	tok_bracketLeft,          	// [
	tok_bracketRight,         	// ]
	tok_comma,                	// ,
	tok_semicolon,            	// ;
	tok_dot,                  	// .
	tok_kwBreak,
	// Primary
			tok_identifier,
	tok_number,
	tok_string,

	// Error
			tok_error
};

class sfeLexan {
private:
	std::map<std::string, Token> keyWords = {
			{"program",   tok_kwProgram},
			{"begin",     tok_kwBegin},
			{"end",       tok_kwEnd},
			{"exit",      tok_kwExit},
			{"const",     tok_kwConst},
			{"var",       tok_kwVar},
			{"array",     tok_kwArray},
			{"for",       tok_kwFor},
			{"do",        tok_kwDo},
			{"to",        tok_kwTo},
			{"downto",    tok_kwDownTo},
			{"by",        tok_kwBy},
			{"if",        tok_kwIf},
			{"or",        tok_kwOr},
			{"and",       tok_kwAnd},
			{"then",      tok_kwThen},
			{"else",      tok_kwElse},
			{"of",        tok_kwOf},
			{"function",  tok_kwFunction},
			{"procedure", tok_kwProcedure},
			{"forward",   tok_kwForward},
			{"integer",   tok_kwInteger},
			{"div",       tok_kwDivide},
			{"mod",       tok_kwModulo},
			{"while",     tok_kwWhile},
			{"string",	tok_kwString},
			{"break",	tok_kwBreak}
	};
	std::string IdentifierStr; // Filled in if tok_identifier
	int NumVal;             // Filled in if tok_number
	std::string TextStr;
	std::ifstream input;
  public:
	std::string getIdentifierStr();
	int getNumVal();
	std::string getTextStr();
	/// gettok - Return the next token from standard input.
	Token gettok();

	sfeLexan(std::string filename);
};

#endif //SFE_LEXAN_H
